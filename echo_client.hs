{-# LANGUAGE OverloadedStrings #-}
-- Echo client program
-- From http://hackage.haskell.org/package/network-3.1.2.1/docs/Network-Socket.html

module Main (main) where

import qualified Control.Exception as E
import qualified Data.ByteString.Char8 as C
import Network.Socket (
    addrAddress,
    addrSocketType,
    close,
    connect,
    defaultHints,
    getAddrInfo,
    HostName,
    openSocket,
    ServiceName,
    Socket,
    SocketType( Stream ),
    withSocketsDo)
import Network.Socket.ByteString (recv, sendAll)

main :: IO ()
main = runTCPClient "127.0.0.1" "3000" $ \s -> do
    sendAll s "Hello, world!"
    msg <- recv s 1024
    putStr "Received: "
    C.putStrLn msg

-- from the "network-run" package.
runTCPClient :: HostName -> ServiceName -> (Socket -> IO a) -> IO a
runTCPClient host port client = withSocketsDo $ do
    addr <- resolve
    E.bracket (open addr) close client
  where
    resolve = do
        let hints = defaultHints { addrSocketType = Stream }
        head <$> getAddrInfo (Just hints) (Just host) (Just port)
    open addr = E.bracketOnError (openSocket addr) close $ \sock -> do
        connect sock $ addrAddress addr
        return sock